import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatButtonModule} from '@angular/material/button';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {MatInputModule} from '@angular/material/input';
import {FormsModule, ReactiveFormsModule } from '@angular/forms';
import {MatSelectModule} from '@angular/material/select';
import {MatMenuModule} from '@angular/material/menu';
import {MatTableModule} from '@angular/material/table';
import {MatTooltipModule} from '@angular/material/tooltip';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {MatCardModule} from '@angular/material/card';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatListModule} from '@angular/material/list';
import {MatGridListModule} from '@angular/material/grid-list';
import {MatTabsModule} from '@angular/material/tabs';
import {MatIconModule} from '@angular/material/icon';
import {MatDialogModule} from '@angular/material/dialog';
import {MatPaginatorModule} from '@angular/material/paginator';

import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import {HttpClient, HttpClientModule} from '@angular/common/http';


import { HomeComponent } from './pages/home/home.component';
import { NavigationComponent } from './components/navigation/navigation.component';
import { AboutComponent } from './pages/about/about.component';
import { ContactComponent } from './pages/contact/contact.component';
import { FaqComponent } from './pages/faq/faq.component';
import { ScheduleComponent } from './components/schedule/schedule.component';
import { ShowScheduleComponent } from './pages/show-schedule/show-schedule.component';
import { SearchScheduleComponent } from './components/search-schedule/search-schedule.component';
import { FooterComponent } from './components/footer/footer.component';
import { TeacherScheduleComponent } from './components/teacher-schedule/teacher-schedule.component';
import { LoginComponent } from './pages/admin/login/login.component';
import { AdminNavigationComponent } from './components/admin/admin-navigation/admin-navigation.component';
import { AdminHomeComponent } from './pages/admin/admin-home/admin-home.component';
import { UserInfoComponent } from './components/admin/user-info/user-info.component';
import { AdminUniversitiesComponent } from './pages/admin/admin-universities/admin-universities.component';
import { CreateUniversityComponent } from './components/admin/create-university/create-university.component';
import { UniversitiesListComponent } from './components/admin/universities-list/universities-list.component';
import { UniversityDeleteDialogComponent } from './dialogs/university-delete-dialog/university-delete-dialog.component';
import { UniversityShowComponent } from './pages/admin/university-show/university-show.component';
import { UniversityEditComponent } from './components/admin/university-edit/university-edit.component';
import { TeachersListComponent } from './pages/admin/teachers-list/teachers-list.component';
@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    NavigationComponent,
    AboutComponent,
    ContactComponent,
    FaqComponent,
    ScheduleComponent,
    ShowScheduleComponent,
    SearchScheduleComponent,
    FooterComponent,
    TeacherScheduleComponent,
    LoginComponent,
    AdminNavigationComponent,
    AdminHomeComponent,
    UserInfoComponent,
    AdminUniversitiesComponent,
    CreateUniversityComponent,
    UniversitiesListComponent,
    UniversityDeleteDialogComponent,
    UniversityShowComponent,
    UniversityEditComponent,
    TeachersListComponent,
    ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatToolbarModule,
    MatButtonModule,
    MatAutocompleteModule,
    MatFormFieldModule,
    MatInputModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    MatSelectModule,
    MatMenuModule,
    MatTableModule,
    MatTooltipModule,
    MatSlideToggleModule,
    MatProgressSpinnerModule,
    MatCardModule,
    MatSnackBarModule,
    MatSidenavModule,
    MatListModule,
    MatGridListModule,
    MatTabsModule,
    MatIconModule,
    MatDialogModule,
    MatPaginatorModule,
    TranslateModule.forRoot({
        loader: {
            provide: TranslateLoader,
            useFactory: HttpLoaderFactory,
            deps: [HttpClient]
        }
    })
  ],
  entryComponents: [
    UniversityDeleteDialogComponent
  ],
  providers: [
    UniversitiesListComponent
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}
