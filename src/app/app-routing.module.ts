import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './pages/home/home.component';
import { AboutComponent } from './pages/about/about.component';
import { ContactComponent } from './pages/contact/contact.component';
import { FaqComponent } from './pages/faq/faq.component';
import { ShowScheduleComponent } from './pages/show-schedule/show-schedule.component';
import { LoginComponent } from './pages/admin/login/login.component';
import { AdminHomeComponent } from './pages/admin/admin-home/admin-home.component';
import { AdminUniversitiesComponent } from './pages/admin/admin-universities/admin-universities.component';
import { UniversityShowComponent } from './pages/admin/university-show/university-show.component';
import { TeachersListComponent } from './pages/admin/teachers-list/teachers-list.component';

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'about', component: AboutComponent },
  { path: 'contact', component: ContactComponent },
  { path: 'faq', component: FaqComponent },
  { path: 'schedule', component: ShowScheduleComponent },
  { path: 'schedule/:param', component: ShowScheduleComponent, runGuardsAndResolvers: 'paramsOrQueryParamsChange', },
  { path: 'login', component: LoginComponent },
  { path: 'admin', component: AdminHomeComponent },
  { path: 'admin/university', component: AdminUniversitiesComponent },
  { path: 'admin/university/:id', component: UniversityShowComponent },
  { path: 'admin/teacher', component: TeachersListComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {onSameUrlNavigation: 'reload'})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
