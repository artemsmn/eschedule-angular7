import { Component, OnInit } from '@angular/core';
import { StaticTextService } from 'src/app/services/staticText.service';
import { HttpParams } from '@angular/common/http';

@Component({
  selector: 'app-faq',
  templateUrl: './faq.component.html',
  styleUrls: ['./faq.component.scss']
})
export class FaqComponent implements OnInit {

  constructor(
  private staticTextService: StaticTextService,
  ) { }
  
    static: any;

  ngOnInit() {
      this.getStaticText();
  }


  /**
   * *Get static text
   */
  getStaticText() {
    const params = new HttpParams().set('name', 'faq');
    this.staticTextService.getStaticText(params)
    .subscribe( (data: any) => {
        this.static = data[0];
        this.showStatic();
    });
  }
  
    showStatic() {
        console.log(this.static);
  }
  

}