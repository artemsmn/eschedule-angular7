import { Component, OnInit, OnDestroy } from '@angular/core';
import {ActivatedRoute, Router, NavigationEnd} from '@angular/router';
import {Subscription} from 'rxjs';
import { UniversityService } from 'src/app/services/university.service';
import { GroupService } from 'src/app/services/group.service';
import { HttpParams } from '@angular/common/http';
import { TeacherService } from 'src/app/services/teacher.service';

@Component({
  selector: 'app-show-schedule',
  templateUrl: './show-schedule.component.html',
  styleUrls: ['./show-schedule.component.scss']
})
export class ShowScheduleComponent implements OnInit, OnDestroy {

  private subscription: Subscription;
  grid: any; // group id
  tid: any; // teacher id
  univ: any;
  group: any;
  teacher: any;
  navigationSubscription;
  isActive = 0;
  isLoaded = false;

  constructor(
    private activateRoute: ActivatedRoute,
    private router: Router,
    private univService: UniversityService,
    private groupService: GroupService,
    private teacherService: TeacherService,
  ) {
    /**
     * heh, https://medium.com/engineering-on-the-incline/reloading-current-route-on-click-angular-5-1a1bfc740ab2
     */
    this.grid = this.activateRoute.snapshot.paramMap.get('group');
    this.tid = this.activateRoute.snapshot.paramMap.get('teacher');
    this.navigationSubscription = this.router.events.subscribe((e: any) => {
      if (e instanceof NavigationEnd) {
        this.initialisePage();
      }
    });
  }

  /**
   * When link params changed call this method
   */
  initialisePage() {
    this.grid = this.activateRoute.snapshot.paramMap.get('group');
    this.tid = this.activateRoute.snapshot.paramMap.get('teacher');
    // console.log(this.tid);
    this.isActive = 0;
    this.isLoaded = false;
    this.ngOnInit();
  }

  ngOnInit() {
    if (this.grid !== null) {
      this.isActive = 1;
    }
    if (this.tid !== null) {
      this.isActive = 1;
    }
    this.getUniv();
  }

  ngOnDestroy() {
    if (this.navigationSubscription) {
       this.navigationSubscription.unsubscribe();
    }
  }

  /**
   * Get data about university
   */
  getUniv() {
    if (this.grid) {
      const params = new HttpParams().set('id', this.grid);
      this.groupService.getGroups(params).subscribe((group: any) => {
        const par = new HttpParams().set('id', group[0].uid);
        this.group = group[0];
        this.univService.getUniv(par).subscribe((univ: any) => {
          this.univ = univ[0];
          this.isLoaded = true;
        });
      });
    }
    if (this.tid) {
      const params = new HttpParams().set('id', this.tid);
      this.teacherService.getTeachers(params).subscribe((teacher: any) => {
      const par = new HttpParams().set('id', teacher[0].uid);
      this.teacher = teacher[0];
      this.univService.getUniv(par).subscribe((univ: any) => {
        this.univ = univ[0];
        this.isLoaded = true;
        });
      });
    }
  }

  /**
   * Navigate to university page
   */
  toUniv() {
    console.log('Go to University with id:' + this.univ.id);
  }
}
