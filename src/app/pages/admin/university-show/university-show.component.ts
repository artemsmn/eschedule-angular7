import { Component, OnInit } from '@angular/core';
import { AdminAccessService } from 'src/app/services/admin-access.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-university-show',
  templateUrl: './university-show.component.html',
  styleUrls: ['./university-show.component.scss']
})
export class UniversityShowComponent implements OnInit {

  constructor(
    private accessService: AdminAccessService,
    private route: ActivatedRoute
  ) { }

  univId: any;

  ngOnInit() {
    this.accessService.checkSuperAdmin();
    this.univId = this.route.snapshot.paramMap.get('id');
  }

}
