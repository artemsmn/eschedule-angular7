import { Component, OnInit, Output, EventEmitter, ViewChild } from '@angular/core';
import { TeacherService } from 'src/app/services/teacher.service';
import { SnackService } from 'src/app/services/snack.service';
import { PageEvent, MatPaginator } from '@angular/material';
import { HttpParams } from '@angular/common/http';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-teachers-list',
  templateUrl: './teachers-list.component.html',
  styleUrls: ['./teachers-list.component.scss']
})
export class TeachersListComponent implements OnInit {

  constructor(
    private teacherService: TeacherService,
    private snackService: SnackService,
    private userService: UserService
  ) { }

  @ViewChild(MatPaginator) paginator: MatPaginator;

  @Output()
  page: EventEmitter<PageEvent>;

  teachers: any;
  isLoading = true;
  isAdmin = false;
  univId = null;
  pagination = {
    pageCount: 0,
    perPage: 0,
    total: 0,
    currentPage: 0
  };
  displayedColumns: string[] = ['id', 'name', 'fullname'];

  ngOnInit() {
    this.checkUser();
    this.paginator.page.subscribe(e => {
      this.getTeachers(e.pageIndex + 1);
    });
  }

  /**
   * Get one page of teachers
   * @param page number of page
   */
  getTeachers(page = '1') {
    this.isLoading = true;
    const params = new HttpParams().set('page', page).set('uid', this.univId);
    this.teacherService.getTeachers(params).subscribe((res: any) => {
      this.teachers = res.body;
      this.pagination = {
        pageCount: res.headers.get('x-pagination-page-count'),
        perPage: res.headers.get('x-pagination-per-page'),
        total: res.headers.get('x-pagination-total-count'),
        currentPage: res.headers.get('x-pagination-current-page')
      };
      this.isLoading = false;
    }, (err) => {
      this.snackService.openSnack('Error!');
      this.isLoading = false;
    });
  }

  /**
   * Check user role
   */
  checkUser() {
    this.userService.getMe().subscribe((res: any) => {
      if (res.role === 'admin') {
        this.isAdmin = true;
        this.displayedColumns.push('university', 'actions');
      } else {
        this.displayedColumns.push('actions');
        this.univId = res.uid;
      }
      this.getTeachers();
    });
  }

  editTeacher(id) {
    console.log('edit:', id);
  }
  deleteTeacher(id, fullname) {
    console.log('delete:', id, fullname);
  }
}
