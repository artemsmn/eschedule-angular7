import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { UserService } from 'src/app/services/user.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  constructor(
    private userService: UserService,
    private router: Router,
    private snackBar: MatSnackBar
  ) { }

  email = new FormControl('', [
    Validators.required,
    Validators.email
  ]);
  password = new FormControl('', [
    Validators.required
  ]);

  ngOnInit() {
  }

  login() {
    const data = {
      email: this.email.value,
      password: this.password.value
    };
    this.userService.login(data)
      .subscribe((response: any) => {
        if (response.access_token) {
          localStorage.setItem('access_token', response.access_token);
          this.router.navigate(['/admin']);
        } else {
          this.snackBar.open('Invalid Credentials', null, {
            duration: 3000,
            verticalPosition: 'top',
            horizontalPosition: 'right',
          });
        }
      }, error => {
        this.loginError(error);
      });
  }

  loginError(error) {
    this.snackBar.open(error.error.message, null, {
      duration: 3000,
      verticalPosition: 'top',
      horizontalPosition: 'right',
    });
  }
}
