import { Component, OnInit } from '@angular/core';
import { AdminAccessService } from 'src/app/services/admin-access.service';

@Component({
  selector: 'app-admin-home',
  templateUrl: './admin-home.component.html',
  styleUrls: ['./admin-home.component.scss']
})
export class AdminHomeComponent implements OnInit {

  constructor(
    private accessService: AdminAccessService
  ) { }

  ngOnInit() {
    this.accessService.checkAccess();
  }

}
