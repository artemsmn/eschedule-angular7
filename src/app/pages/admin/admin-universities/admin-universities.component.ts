import { Component, OnInit } from '@angular/core';
import { AdminAccessService } from 'src/app/services/admin-access.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-admin-universities',
  templateUrl: './admin-universities.component.html',
  styleUrls: ['./admin-universities.component.scss']
})
export class AdminUniversitiesComponent implements OnInit {

  constructor(
    private accessService: AdminAccessService,
    private router: Router
  ) { }

  ngOnInit() {
    this.accessService.checkSuperAdmin();
  }

}
