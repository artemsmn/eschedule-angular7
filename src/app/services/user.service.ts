import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { COMMON_URL} from './common.url';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) { }

  /**
   * Login to system
   * @param body - body of request
   * @param headers - headers
   */
  login(body) {
    return this.http.post(COMMON_URL.user.login, body);
  }

  /**
   * Get user data by access token
   */
  getMe() {
    const token = localStorage.getItem('access_token');
    const headers = {
      headers: new HttpHeaders({
        Authorization: 'Bearer ' + token
      })
    };
    return this.http.get(COMMON_URL.user.checkAccess, headers);
  }

  /**
   * Create new user
   */
  create(body) {
    const token = localStorage.getItem('access_token');
    const headers = {
      headers: new HttpHeaders({
        Authorization: 'Bearer ' + token
      })
    };
    return this.http.post(COMMON_URL.user.create, body, headers);
  }
}
