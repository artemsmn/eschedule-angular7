import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { COMMON_URL } from '../services/common.url';

@Injectable({
  providedIn: 'root'
})
export class UniversityService {

  constructor(private http: HttpClient) { }
  /**
   * *Get requests for universities
   * @param par - params of request
   */
  getUniv(par) {
    return this.http.get(COMMON_URL.univ.index, { params: par });
  }

  /**
   * Create new university
   */
  create(body) {
    const token = localStorage.getItem('access_token');
    const headers = {
      headers: new HttpHeaders({
        Authorization: 'Bearer ' + token,
      })
    };
    return this.http.post(COMMON_URL.univ.create, body, headers);
  }

  /**
   * Get single university
   * @param id Universuty id
   */
  show(id) {
    const token = localStorage.getItem('access_token');
    const headers = {
      headers: new HttpHeaders({
        Authorization: 'Bearer ' + token,
      })
    };
    return this.http.get(COMMON_URL.univ.show + id, headers);
  }

  /**
   * Update one University
   * @param id Universuty id
   */
  update(id, body) {
    const token = localStorage.getItem('access_token');
    const headers = {
      headers: new HttpHeaders({
        Authorization: 'Bearer ' + token,
      })
    };
    return this.http.put(COMMON_URL.univ.update + id, body, headers);
  }
  /**
   * Delete exiting university
   * @param id University id
   */
  delete(id) {
    const token = localStorage.getItem('access_token');
    const headers = {
      headers: new HttpHeaders({
        Authorization: 'Bearer ' + token,
      })
    };
    return this.http.delete(COMMON_URL.univ.delete + id, headers);
  }
}
