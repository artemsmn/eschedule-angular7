import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { COMMON_URL } from '../services/common.url';
import { HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AdminAccessService {

  constructor(
    private http: HttpClient,
    private router: Router
  ) { }

  /**
   * Check user token
   */
  checkAccess() {
    const token = localStorage.getItem('access_token');
    const headers = {
      headers: new HttpHeaders({
        Authorization: 'Bearer ' + token
      })
    };
    this.http.get(COMMON_URL.user.checkAccess, headers)
      .subscribe(response => {
        return response;
      }, error => {
        this.router.navigate(['/login']);
      });
  }

  checkSuperAdmin() {
    const token = localStorage.getItem('access_token');
    const headers = {
      headers: new HttpHeaders({
        Authorization: 'Bearer ' + token
      })
    };
    this.http.get(COMMON_URL.user.checkAccess, headers)
      .subscribe(( response: any) => {
        if (response.role !== 'admin') {
          this.router.navigate(['/admin']);
        }
      }, error => {
        this.router.navigate(['/admin']);
      });
  }
}
