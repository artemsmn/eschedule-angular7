import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { COMMON_URL} from '../services/common.url';

@Injectable({
  providedIn: 'root'
})
export class GroupService {

  constructor(private http: HttpClient) { }
  /**
   * *Get request for groups
   * @param par - params of request
   */
  getGroups(par) {
    return this.http.get(COMMON_URL.group, { params: par });
  }
}
