import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { COMMON_URL} from '../services/common.url';

@Injectable({
  providedIn: 'root'
})
export class TeacherService {

  constructor(private http: HttpClient) { }

  /**
   * Index method
   * @param par params of request
   */
  getTeachers(par) {
    return this.http.get(COMMON_URL.teacher.index, { params: par, observe: 'response' });
   }

   /**
    * Craete teacher
    * @param body Body of request
    */
   create(body) {
    const token = localStorage.getItem('access_token');
    const headers = {
      headers: new HttpHeaders({
        Authorization: 'Bearer ' + token,
      })
    };
    return this.http.post(COMMON_URL.teacher.create, body, headers);
   }

   /**
    * Show single teacher
    * @param id Teacher id
    */
   show(id) {
    const token = localStorage.getItem('access_token');
    const headers = {
      headers: new HttpHeaders({
        Authorization: 'Bearer ' + token,
      })
    };
    return this.http.post(COMMON_URL.teacher.show + id, headers);
   }

   /**
    * Update single teacher
    * @param id Teacher id
    * @param body Body of request
    */
   update(id, body) {
    const token = localStorage.getItem('access_token');
    const headers = {
      headers: new HttpHeaders({
        Authorization: 'Bearer ' + token,
      })
    };
    return this.http.put(COMMON_URL.teacher.update + id, body, headers);
   }

   /**
    * Delete single teacher
    * @param id Teacher id
    */
   delete(id) {
    const token = localStorage.getItem('access_token');
    const headers = {
      headers: new HttpHeaders({
        Authorization: 'Bearer ' + token,
      })
    };
    return this.http.delete(COMMON_URL.teacher.update + id, headers);
   }

}
