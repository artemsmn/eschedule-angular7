const api = 'https://eschedule.collapse.ml/api';

export const COMMON_URL = {
  univ: {
    index: api + '/university',
    create: api + '/university',
    delete: api + '/university/',
    show: api + '/university/',
    update: api + '/university/'
  },
  group: api + '/group',
  static: api + '/static-text',
  lesson: {
    index: api + '/lesson',
    group: api + '/lesson/group',
    teacher: api + '/lesson/teacher'
  },
  teacher: {
    index: api + '/teacher',
    create: api + '/university',
    delete: api + '/university/',
    show: api + '/university/',
    update: api + '/university/'
  },
  user: {
    login: api + '/user/login',
    checkAccess: api + '/user/get-by-token',
    create: api + '/user'
  }
};
