import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { COMMON_URL} from '../services/common.url';

@Injectable({
  providedIn: 'root'
})
export class StaticTextService {

  constructor(private http: HttpClient) { }
  /**
   * *Get requests for static text
   * @param par - params of request
   */
  getStaticText(par) {
    return this.http.get(COMMON_URL.static, { params: par });
  }
}
