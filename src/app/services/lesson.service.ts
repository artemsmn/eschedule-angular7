import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { COMMON_URL} from '../services/common.url';

@Injectable({
  providedIn: 'root'
})
export class LessonService {

  constructor(private http: HttpClient) { }
  /**
   * *Get lessons
   * @param par params of request
   */
  getLesson(par) {
    return this.http.get(COMMON_URL.lesson.group, { params: par });
  }
  getLessonByTeacher(par) {
    return this.http.get(COMMON_URL.lesson.teacher, { params: par });
  }
}
