import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-admin-navigation',
  templateUrl: './admin-navigation.component.html',
  styleUrls: ['./admin-navigation.component.scss']
})
export class AdminNavigationComponent implements OnInit {

  constructor(
    private userService: UserService,
    private router: Router
  ) { }

  isAdmin = false;
  isLoaded = false;
  ngOnInit() {
    this.checkAdmin();
  }

  checkAdmin() {
    this.userService.getMe().subscribe( (res: any) => {
    if (res.role === 'admin') {
      this.isAdmin = true;
    }
    this.isLoaded = true;
    });
  }

  logout() {
    localStorage.removeItem('access_token');
    this.router.navigate(['/login']);
  }

}
