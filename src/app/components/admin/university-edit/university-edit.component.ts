import { Component, OnInit, Input } from '@angular/core';
import { AdminAccessService } from 'src/app/services/admin-access.service';
import { UniversityService } from 'src/app/services/university.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { SnackService } from 'src/app/services/snack.service';

@Component({
  selector: 'app-university-edit',
  templateUrl: './university-edit.component.html',
  styleUrls: ['./university-edit.component.scss']
})
export class UniversityEditComponent implements OnInit {

  constructor(
    private accessService: AdminAccessService,
    private univService: UniversityService,
    private snackService: SnackService
  ) { }

  @Input() univId: any;
  university: any;
  isLoading = false;

  univForm = new FormGroup({
    name: new FormControl('', [
      Validators.required
    ]),
    fullname: new FormControl('', [
      Validators.required
    ]),
    zero: new FormControl('', []),
    first: new FormControl('', []),
    second: new FormControl('', []),
    third: new FormControl('', []),
    fourth: new FormControl('', []),
    fifth: new FormControl('', []),
  });

  ngOnInit() {
    this.accessService.checkSuperAdmin();
    this.getUniv();
  }

  /**
   * Get university data and fill all inputs
   */
  getUniv() {
    this.univService.show(this.univId).subscribe((res: any) => {
      this.university = res;
      this.univForm.setValue({
        name: this.university.name,
        fullname: this.university.fullname,
        zero: this.university.zero,
        first: this.university.first,
        second: this.university.second,
        third: this.university.third,
        fourth: this.university.fourth,
        fifth: this.university.fifth
      });
    }, (err: any) => {
      console.log(err);
    });
  }

  /**
   * Save data to DB
   */
  onSubmit() {
    this.isLoading = true;
    this.univService.update(this.univId, this.univForm.value).subscribe( (res: any) => {
      this.snackService.openSnack('University updated!');
      this.getUniv();
      this.isLoading = false;
    }, (err: any) => {
      this.snackService.openSnack('Error!');
      this.isLoading = false;
    });
  }
}
