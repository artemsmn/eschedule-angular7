import { Component, OnInit } from '@angular/core';
import { UniversityService } from 'src/app/services/university.service';
import { HttpParams } from '@angular/common/http';
import { MatDialog } from '@angular/material';
import { UniversityDeleteDialogComponent } from 'src/app/dialogs/university-delete-dialog/university-delete-dialog.component';
import { SnackService } from 'src/app/services/snack.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-universities-list',
  templateUrl: './universities-list.component.html',
  styleUrls: ['./universities-list.component.scss']
})
export class UniversitiesListComponent implements OnInit {

  constructor(
    private univService: UniversityService,
    public dialog: MatDialog,
    private snackService: SnackService,
    private router: Router
  ) { }

  tableData;
  displayedColumns: string[] = ['id', 'name', 'fullname', 'admin', 'actions'];

  ngOnInit() {
    this.getUniversities();
  }

  getUniversities() {
    const params = new HttpParams().set('fields', 'id,name,fullname,admin');
    this.univService.getUniv(params).subscribe( (res: any) => {
      this.tableData = res;
    }, (err: any) => {
      console.log('error');
    });
  }

  deleteUniv(id, fullname) {
    console.log('delete', id);
    const dialogRef = this.dialog.open(UniversityDeleteDialogComponent, {
      width: '400px',
      data: {id, fullname}
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.univService.delete(result).subscribe( (res: any) => {
          this.snackService.openSnack('University deleted');
          this.getUniversities();
        }, (err: any) => {
          this.snackService.openSnack('Error!');
          this.getUniversities();
        });
      }
    });
  }

  editUniv(id) {
    this.router.navigate(['admin/university', id]);
  }
}
