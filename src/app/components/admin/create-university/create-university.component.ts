import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material';
import { UniversityService } from 'src/app/services/university.service';
import { UserService } from 'src/app/services/user.service';
import { SnackService } from 'src/app/services/snack.service';
import { UniversitiesListComponent } from '../universities-list/universities-list.component';

@Component({
  selector: 'app-create-university',
  templateUrl: './create-university.component.html',
  styleUrls: ['./create-university.component.scss']
})
export class CreateUniversityComponent implements OnInit {

  constructor(
    private univListCom: UniversitiesListComponent,
    private univService: UniversityService,
    private userService: UserService,
    private snackService: SnackService
  ) { }

  univForm = new FormGroup({
    name: new FormControl('', [
      Validators.required
    ]),
    fullname: new FormControl('', [
      Validators.required
    ]),
    email: new FormControl('', [
      Validators.email,
      Validators.required
    ]),
    password: new FormControl({ value: '', disabled: true }, [
      Validators.required
    ]),
    adminFirstName: new FormControl('', [
      Validators.required
    ]),
    adminLastName: new FormControl('', [
      Validators.required
    ]),
  });
  isSubmit = false;
  validPass = false;
  ngOnInit() {
  }

  onSubmit() {
    const univData = {
      name: this.univForm.get('name').value,
      fullname: this.univForm.get('fullname').value,
      sef: this.univForm.get('name').value,
      admin: this.univForm.get('adminFirstName').value.toLowerCase() + '.' + this.univForm.get('adminLastName').value.toLowerCase()
    };
    const userData = {
      email: this.univForm.get('email').value,
      password: this.univForm.get('password').value,
      username: this.univForm.get('adminFirstName').value.toLowerCase() + '.' + this.univForm.get('adminLastName').value.toLowerCase()
    };
    this.univService.create(univData).subscribe((res: any) => {
      this.snackService.openSnack('University created!');
      this.userService.create(userData).subscribe((userRes: any) => {
        this.snackService.openSnack('User created!');
        window.location.reload();
      }, (err: any) => {
        this.snackService.openSnack('Error on creating user');
      });
    }, (err: any) => {
      this.snackService.openSnack('Error on creating university');
    });
  }

  genPass() {
    return Math.random().toString(36).slice(-10);
  }
  refreshPass() {
    this.univForm.setValue(Object.assign(this.univForm.value, { password: this.genPass() }));
    this.validPass = true;
  }

}
