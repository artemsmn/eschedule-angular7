import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/user.service';
import { UniversityService } from 'src/app/services/university.service';
import { HttpParams } from '@angular/common/http';

@Component({
  selector: 'app-user-info',
  templateUrl: './user-info.component.html',
  styleUrls: ['./user-info.component.scss']
})
export class UserInfoComponent implements OnInit {

  constructor(
    private userService: UserService,
    private universityService: UniversityService
  ) { }

  user: any;
  university: any;
  isLoaded = false;
  ngOnInit() {
    this.getUser();
  }

  getUser() {
    this.userService.getMe().subscribe((response: any) => {
      this.user = response;
      if (this.user.uid) {
        const params = new HttpParams().set('id', response.uid);
        this.universityService.getUniv(params).subscribe(univData => {
          this.university = univData[0];
          this.isLoaded = true;
        });
      } else {
        this.isLoaded = true;
        this.university = null;
      }
    });
  }
}
