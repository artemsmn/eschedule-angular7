import { Component, OnInit, Input } from '@angular/core';
import { LessonService } from '../../services/lesson.service';
import { HttpParams } from '@angular/common/http';
import { GroupService } from 'src/app/services/group.service';
import { UniversityService } from 'src/app/services/university.service';

@Component({
  selector: 'app-schedule',
  templateUrl: './schedule.component.html',
  styleUrls: ['./schedule.component.scss']
})
export class ScheduleComponent implements OnInit {
  @Input() grid: any;
  @Input() univ: any;
  lessons: any;
  isLoading = true;
  weekDays = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'];
  displayedColumns: string[] = ['time', 'lesson', 'class', 'teacher'];

  constructor(
    private lessonService: LessonService,
    private groupService: GroupService,
    private univService: UniversityService
    ) { }

  ngOnInit() {
    // this.getUniv();
    // this.isLoading = true;
    this.getLessons(this.grid);
  }

// tslint:disable-next-line: use-life-cycle-interface
  ngOnChanges() {
    // this.getUniv();
    this.isLoading = true;
    this.getLessons(this.grid);
  }

  /**
   * *Get lessons list
   */
   getLessons(gid) {
    const params = new HttpParams().set('id', gid);
    this.lessonService.getLesson(params)
    .subscribe((data: any) => {
      this.lessons = this.processLessons(data);
      this.isLoading = false;
    });
  }

  /**
   * *Divide data to separate days of the week
   * *Change number of lesson to time
   * @param data - array of lessons
   */
  processLessons(data) {
    const response = [];
    data.forEach(element => {
      switch (element.number) {
        case 0: element.number = this.univ.zero; break;
        case 1: element.number = this.univ.first; break;
        case 2: element.number = this.univ.second; break;
        case 3: element.number = this.univ.third; break;
        case 4: element.number = this.univ.fourth; break;
        case 5: element.number = this.univ.fifth; break;
      }
    });
    this.weekDays.forEach((element, index) => {
// tslint:disable-next-line: only-arrow-functions
      response[index] = data.filter(function(wd) {
        return (wd.wd === element);
      });
    });
    return response;
  }

}
