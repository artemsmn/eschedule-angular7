import { Component, OnInit } from '@angular/core';
import { UniversityService } from 'src/app/services/university.service';
import { GroupService } from 'src/app/services/group.service';
import { Router } from '@angular/router';
import { FormControl, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { HttpParams } from '@angular/common/http';
import { startWith, map } from 'rxjs/operators';
import { TeacherService } from 'src/app/services/teacher.service';

@Component({
  selector: 'app-search-schedule',
  templateUrl: './search-schedule.component.html',
  styleUrls: ['./search-schedule.component.scss']
})
export class SearchScheduleComponent implements OnInit {

  constructor(
    private univService: UniversityService,
    private groupService: GroupService,
    private teacherService: TeacherService,
    private router: Router,
    ) {}

  formUniv = new FormControl(null, [
    Validators.required
  ]);
  formGr = new FormControl(null, [
    Validators.required
  ]);
  formTeachers = new FormControl(null, [
    Validators.required
  ]);
  formSlide = new FormControl();
  universities: any;
  groups: any;
  teachers: any;
  filteredUniv: Observable<any[]>;
  filteredGr: Observable<any[]>;
  filteredTeachers: Observable<any[]>;
  selectedUniv: any;

  ngOnInit() {
    this.getUniversities();
    this.toggleChange();
  }

  /**
   * Block one of input
   */
  toggleChange() {
    if (this.formSlide.value) {
      this.formTeachers.enable();
      this.formGr.disable();
    } else {
      this.formTeachers.disable();
      this.formGr.enable();
    }
  }

  /**
   * *Submit form
   */
  getSchedule() {
    let param;
    if (this.formSlide.value) {
      const tName = this.formTeachers.value;
      // tslint:disable-next-line: only-arrow-functions
      const tid = this.teachers.filter(function(element) {
        return element.short_name === tName;
      })[0].id;
      param = {teacher: tid};
    } else {
      const gName = this.formGr.value;
      // tslint:disable-next-line: only-arrow-functions
      const grid = this.groups.filter(function(element) {
            return element.name === gName;
          })[0].id;
      param = {group: grid};
    }
    this.router.navigate(['schedule', param]);
  }

  /**
   * *Action when university was chousen
   * @param uid - university id
   */
  chooseUniv(uid) {
    this.getGroups(uid);
    this.getTeachers(uid);
    this.selectedUniv = this.getUniversity(uid);
  }

  /**
   * *Get single university
   * @param id - university id
   */
  getUniversity(id) {
// tslint:disable-next-line: only-arrow-functions
    return this.universities.filter(function(elem) {
      return elem.id === id;
    })[0];
  }

  /**
   * *Get universites list from server
   */
  getUniversities() {
    const params = new HttpParams().set('fields', 'id,name,zero,first,second,third,fourth,fifth');
    this.univService.getUniv(params)
    .subscribe( (data: any) => {
        this.universities = data;
        this.setFilteredUniv();
    });
  }
  /**
   * *Get groups of university
   * @param uid - university id wich groups we get
   */
  getGroups(uid) {
    const params = new HttpParams().set('fields', 'name,id').set('uid', uid);
    this.groupService.getGroups(params)
    .subscribe( (data: any) => {
        this.groups = data;
        this.setFilteredGroup();
    });
  }

  /**
   * Get teachers of university
   * @param uid - university id wich teachers we get
   */
  getTeachers(uid) {
    const params = new HttpParams().set('fields', 'short_name,id').set('uid', uid);
    this.teacherService.getTeachers(params)
    .subscribe( (data: any) => {
        this.teachers = data;
        this.setFilteredTeachers();
    });
  }

  /**
   * Filter teachers
   */
  setFilteredTeachers() {
    this.filteredTeachers = this.formTeachers.valueChanges
      .pipe(
        startWith(''),
        map(value => value ? this._filter_teachers(value) : this.teachers.slice())
      );
  }

  private _filter_teachers(value: string): string[] {
    const filterValue = value.toLowerCase();
    return this.teachers.filter(option => option.short_name.toLowerCase().includes(filterValue));
  }

  /**
   * *Filter universities
   */
  setFilteredUniv() {
    this.filteredUniv = this.formUniv.valueChanges
      .pipe(
        startWith(''),
        map(value => this._filter_university(value))
      );
  }

  private _filter_university(value: string): string[] {
    const filterValue = value.toLowerCase();
    return this.universities.filter(option => option.name.toLowerCase().includes(filterValue));
  }
  /**
   * *Filter groups
   */
  setFilteredGroup() {
    this.filteredGr = this.formGr.valueChanges
      .pipe(
        startWith(''),
        map(value => value ? this._filter_group(value) : this.groups.slice())
      );
  }

  private _filter_group(value: string): string[] {
    const filterValue = value.toLowerCase();
    return this.groups.filter(option => option.name.toLowerCase().includes(filterValue));
  }
}
