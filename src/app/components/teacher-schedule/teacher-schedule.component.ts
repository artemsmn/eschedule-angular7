import { Component, OnInit, Input } from '@angular/core';
import { HttpParams } from '@angular/common/http';
import { UniversityService } from 'src/app/services/university.service';
import { LessonService } from 'src/app/services/lesson.service';


@Component({
  selector: 'app-teacher-schedule',
  templateUrl: './teacher-schedule.component.html',
  styleUrls: ['./teacher-schedule.component.scss']
})
export class TeacherScheduleComponent implements OnInit {

  @Input() tid: any;
  @Input() univ: any;
  lessons: any;
  isLoading = true;
  weekDays = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'];
  displayedColumns: string[] = ['time', 'lesson', 'class', 'group'];

  constructor(
    private lessonService: LessonService,
    ) { }

  ngOnInit() {
    // this.getUniv();
    this.getLessons(this.tid);
  }

// tslint:disable-next-line: use-life-cycle-interface
  ngOnChanges() {
    // this.getUniv();
    this.isLoading = true;
    this.getLessons(this.tid);
  }

  /**
   * *Get lessons list
   */
   getLessons(gid) {
    const params = new HttpParams().set('id', gid);
    this.lessonService.getLessonByTeacher(params)
    .subscribe((data: any) => {
      this.lessons = this.processLessons(data);
      this.isLoading = false;
    });
  }

  /**
   * *Divide data to separate days of the week
   * *Change number of lesson to time
   * @param data - array of lessons
   */
  processLessons(data) {
    const response = [];
    data.forEach(element => {
      switch (element.number) {
        case 0: element.number = this.univ.zero; break;
        case 1: element.number = this.univ.first; break;
        case 2: element.number = this.univ.second; break;
        case 3: element.number = this.univ.third; break;
        case 4: element.number = this.univ.fourth; break;
        case 5: element.number = this.univ.fifth; break;
      }
    });
    this.weekDays.forEach((element, index) => {
// tslint:disable-next-line: only-arrow-functions
      response[index] = data.filter(function(wd) {
        return (wd.wd === element);
      });
    });
    return response;
  }

}
