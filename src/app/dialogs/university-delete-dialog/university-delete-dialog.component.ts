import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-university-delete-dialog',
  templateUrl: './university-delete-dialog.component.html',
  styleUrls: ['./university-delete-dialog.component.scss']
})
export class UniversityDeleteDialogComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<UniversityDeleteDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) {}

  onNoClick(): void {
    this.dialogRef.close();
  }

  ngOnInit() {
  }

  deleteUniv() {
    this.dialogRef.close(this.data.id);
  }
}
